module.exports = {
  templateFormats: [
    "html",
    "md",
    "css",
    "gif",
    "png",
    "jpg",
    "woff"
  ],
  passthroughFileCopy: true
};
