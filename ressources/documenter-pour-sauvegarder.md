---
layout: page.liquid
title: Documenter pour sauvegarder
tags:
  - approche
---
Documenter pour sauvegarder
=======

Cette page a été, dans un premier temps, mise à jour sans accès à Internet — raison suffisante pour penser et produire [une information en local](/ressources/jouer-a-domicile/) avant sa mise à disposition sur la toile.

## Magie d'une documentation versionnée et ouverte

Le présent site est produit avec Eleventy (11ty) un générateur de site statique. L'avantage est de pouvoir produire sur un ordinateur - en local - une information facilement consultable. Ce site aurait aussi pu être produit simplement avec du texte, des balises sémantiques (HTML) et mis en forme avec des feuilles de styles (CSS). Dans tous les cas ce site est poussé (Git) sur GitLab pour diffusion, correction et amélioration auprès de l'ensemble des internautes. Vous pouvez améliorer cette page ou l'ensemble du site en rejoignant le groupe <a href="https://gitlab.com/fuir-le-cloud">fuir-le-cloud</a> sur GitLab.

L'intérêt de cette démarche est de pouvoir avoir accès à l'ensemble de sa documentation sans connexion et dans un deuxième temps de pouvoir la diffuser.

## Le papier en support

Nous pouvons envisager le papier comme une sauvegarde durable de l'information. La documentation doit pouvoir être produite, diffusée, mise à jour... et imprimable à tout moment pour un besoin de consultation sans avoir recours à l'énergie électrique. C'est ce que, par exemple, l'expérience [Qui a mangé le graphiste ?](https://qui-a-mange-le-graphiste.gitlab.io/) souhaitait offrir. Un autre exemple est la [version papier du site Low-Tech Magazine](https://solar.lowtechmagazine.com/2019/03/printed-website.html) de Kris de Decker.

## Beaker

Faire un point sur le [navigateur Beaker](https://beakerbrowser.com/) et son approche maligne d'un internet P2P.

## À tester

+ [mkdocs](https://www.mkdocs.org/)
+ [Literate](https://zyedidia.github.io/literate/index.html)
+ [Org Mode](https://orgmode.org/index.html)
