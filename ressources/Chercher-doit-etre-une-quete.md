---
layout: page.liquid
title: Chercher doit être une quête
tags:
  - outil
---
Chercher doit être une quête
=======

## Google - le seul et unique

Découvrir Google Search à la fin 90's était une forme de révélation: des résultats de recherche pertinents! Malheureusement la révélation est aujourd'hui hégémonie. Et la recherche est devenu un simple service. Vous cherchez un site: Google! Vous cherchez un itinéraire: Google! Vous cherchez une image: Google! Vous cherchez ... : Google!

## Les knowledge graphs - le mal absolu!

Ce conditionnement pavlovien s'est renforcé avec l'arrivé des *[knowledge graphs](https://en.wikipedia.org/wiki/Google_Knowledge_Graph)*. Il est fréquent d'obtenir une information suffisante dès cette première page de résultats et de ne pas aller plus avant dans une requête. Merci Google d'indexer le Net mais se substituer à l'information est un hold-up. Nous pouvons apprécier l'aide du bibliothécaire, mais ce bibliothécaire ne sera jamais le livre que nous aimerions consulter.

## Des alternatives?

Il existe des [alternatives à Google Search](/ressources/alternatives-a-google-search/).

Il peut être aussi utile de faire appel à plusieurs [moteurs de recherches](/ressources/moteurs-aux-resultats-pertinents/) pour répondre à notre quête d'information.

## La documentation papier

Et n'oublions pas que la documentation existe aussi au format papier. Chercher un mot dans un dictionnaire papier a la beauté de la flânerie.
