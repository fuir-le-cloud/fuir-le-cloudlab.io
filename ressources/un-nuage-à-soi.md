---
layout: page.liquid
title: Un nuage à soi
date: 2016-01-02
tags:
  - approche
---
Un  nuage à soi
=======

## YunoHost

+ Le site [YunoHost](https://yunohost.org/fr) contient la documentation sur ce système d'exploitation libre qui vise à simplifier l'administration d'un serveur et à démocratiser l'auto-hébergement.
+ Un [compte-rendu d'un Atelier Serveur local indépendant](https://www.notion.so/CR-Atelier-Serveur-local-ind-pendant-30-juin-2019-3712bf72cac8484e92f1bc025a7feff0) qui s'est tenu le 30 juin 2019 à Nevez en Bretagne.

## Cultiver son jardin avec FramaCloud

Les tutoriels de la rubrique « [Cultiver son jardin](https://framacloud.org/fr/cultiver-son-jardin/) » concernent les logiciels que Framasoft propose comme services en ligne libres dans le cadre de la campagne de « dégooglisation ». Ils sont destinés à un public qui souhaiterait aller plus loin dans l’émancipation. Si vous n’avez jamais hébergé votre propre site web, ces tutoriels ne seront peut-être pas pour vous. Certains sont simples, d’autres beaucoup moins mais c’est peut-être une excellente occasion d’apprendre. À travers ces tutoriels, nous voulons encourager un maximum de personnes à installer – ou demander de faire installer, ce qui est tout aussi important – chez elles, au sein de leur entreprise, association, établissement scolaire… ces logiciels pour contribuer à la décentralisation du net.

## Outils à installer sur son petit nuage

+ [hedgedoc.org/](https://hedgedoc.org/) - markdown pour écrire et partager.
