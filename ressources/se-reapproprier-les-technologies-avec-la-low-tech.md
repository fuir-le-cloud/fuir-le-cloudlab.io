---
layout: page.liquid
title: Se réapproprier la technologie avec la low-tech
tags:
  - perspective
---
Se réapproprier les technologies avec la low-tech
=======

## Quelques ressources

+ [Low tech : face au tout-numérique, se réapproprier les technologies](https://www.coredem.info/rubrique85.html) - Ce nouveau numéro de la collection Passerelle s’inscrit dans le cadre des réflexions, sociales et politiques, de plus en plus nombreuses autour de la question des low tech.
+ (pas encore lu) [La vie low-tech en 2040](https://www.institutparisregion.fr/economie/commerce-et-consommation/la-vie-low-tech-en-2040/)
