---
layout: page.liquid
title: Jouer à domicile
tags:
  - approche
---
Jouer à domicile
=======

En mai 2020 (un effet du confinement?) Geoffroy Dorne expliquait [comment il avait sauvegardé son web sur un disque dur](https://graphism.fr/comment-jai-sauvegarde-mon-web-sur-un-disque-dur/).

Démarche intéressante mais comment conjuguer cela avec un [partage de pair à pair](/ressources/echanges-de-pair-a-pair/)?
