---
layout: page.liquid
title: Fausses promesses d'une connexion obligée
date: 2021-04-02
tags:
  - perspective
---
Fausses promesses d'une connexion obligée
=======

Promettre monts et merveilles en échange d'un compte utilisateur peut signifier le début d'une surveillance accrue. Mais peut-on profiter des avantages d'Internet sans jamais créer le moindre compte utilisateur?

## Du compte utilisateur à l'exploitation CRM

Difficile de croire que la création d'un compte utilisateur ne vous fait pas obligatoirement tomber dans la base de données prospects/clients (Customer Relationship Management) qui sera exploitée de toute façon, ne serait-ce que pour une relance par mail. Même le plus vertueux des entrepreneurs ne supporte pas de vous voir inactif sur son produit, son service, sa plateforme...

La question qui reste ouverte est donc: comment utiliser Internet et ses services sans ce besoin constant de se connecter?
