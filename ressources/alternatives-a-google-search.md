---
layout: page.liquid
title: Alternatives à Google Search
tags:
  - outil
---
Alternatives à Google Search
=======

Quelque soient les raisons pour lesquelles nous voudrions fuir Google, voilà quelques alternatives à son moteur de recherche.

(Note: faire un point sur les technologies qui se cachent derrière chaque moteur)

## DuckDuckGo ne collecte pas les informations personnelles

Ajoutez une recherche web privée à votre navigateur préféré, ou recherchez directement sur [duckduckgo.com](https://duckduckgo.com/). Leur politique de confidentialité est simple: ils ne collectent ni ne partagent nos informations personnelles. À aucun moment ils nous demanderons de créer un compte.

L'[appli mobile DuckDuckGo](https://duckduckgo.com/app) est très bien pensée avec sa fonctionnalité "incinérateur" pour supprimer l'ensemble de ses traces en fin de session.

La recherche images sur DuckDuckGo est loin d'être aussi efficace que Google images. Des idées d'alternatives à Google Images?

## Ecosia, le moteur qui marche à l'énergie renouvelable

L'équipe marketing d'[ecosia.org  ](https://www.ecosia.org/) annonce que grâce à leur propres centrales photovoltaïques, ils ont produit suffisamment d'énergie propre pour alimenter à 100 % toutes les recherches Ecosia par de l'énergie renouvelable. Peuvent-ils pour autant parler de [Bilan carbone négatif](https://fr.blog.ecosia.org/pourquoi-un-bilan-carbone-neutre-ne-suffit-pas-ecosia-a-installe-ses-propres-panneaux-solaires/)?

## Lilo, le moteur de recherche français et solidaire

[Lilo](https://search.lilo.org/), le moteur de recherche français et solidaire qui finance gratuitement les projets de votre choix. Et parce que votre vie privée est importante, Lilo protège vos données personnelles.

Concrètement, comment ça marche? J’effectue mes recherches internet comme d’habitude. A chaque recherche, je reçois une goutte d’eau Lilo. Je distribue mes gouttes d’eau aux projets solidaires de mon choix. Lilo reverse aux projets l'argent correspondant aux gouttes d'eau. Mais pour garder une trace de mes gouttes d'eau, il va me falloir ouvrir un compte. Dommage.

## Green SearX, l'instance SearX de la Green Web Foundation

[Searx](https://searx.github.io/searx/) est un méta-moteur qui agrège les résultats d'autres moteurs de recherche sans stocker les informations des utilisateurs. L'[instance Searx de la Green Web Foundation](https://searx.thegreenwebfoundation.org/) va plus loin en proposant de ne proposer comme résultats que des sites fonctionnant à l'énergie verte.

## Qwant, Le moteur de recherche qui respecte votre vie privée

Contrairement à Ecosia et Lilo, des moteurs de recherche à supplément éthique, [Qwant](https://www.qwant.com/) rejoint DuckDuckGo dans cette volonté de défendre l'anonymat des utilisateurs.

## Mojeek

[Mojeek](https://www.mojeek.com/) is a growing independent search engine which does not track you.

## Des moteurs spécialisés donc pertinents

Vous pouvez utiliser des [Moteurs de recherches spécialisés](/ressources/moteurs-aux-resultats-pertinents/) pour obtenir des résultats plus pertinents.
