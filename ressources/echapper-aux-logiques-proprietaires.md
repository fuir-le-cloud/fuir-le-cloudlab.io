---
layout: page.liquid
title: Échapper aux logiques propriétaires
tags:
  - approche
---
Échapper aux logiques propriétaires
=======

## Le bon exemple de Notion

[Notion](https://www.notion.so/) est un outil pratique de documentation qui est respectueux des données que vous lui confiées. Si vous voulez récupérer ses données et les mettre à jour avec d'autres outils, faites un export en md et csv (menu en haut à droite) et voilà! Merci Notion.

Un Notion (payant à partir d'un certain poids de données) peut faire office de site de communication ouvert à tous. (à documenter)
