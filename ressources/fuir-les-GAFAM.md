---
layout: page.liquid
title: Fuir les GAFAM
tags:
  - outil
---
Fuir les GAFAM
=======

## Ressources qui listent des alternatives aux GAFAM.

+ En route vers votre indépendance numérique? La voie est libre! [Dégooglisons Internet](https://degooglisons-internet.org/fr/) vous propose de la cheminer en plusieurs étapes.
+ [No More Google](https://nomoregoogle.com/) — Privacy-friendly alternatives to Google that don't track you
+ [Privacy and Security Tools for Beginners](https://thistooshallgrow.com/blog/privacy-security-tools-beginners)
+ [Good Reports](https://goodreports.com/) - Get off Big Tech tools. Use these instead
+ [Switching Software](https://switching.software/) - Ethical, easy-to-use and privacy-conscious alternatives to well-known software
+ [Awesome Selfhosted](https://github.com/balzack/awesome-selfhosted) - This is a list of Free Software network services and web applications which can be hosted on your own server(s).
+ [Liste non exhaustive d'alternatives aux produits et services des GAFAM](https://francoischarlet.ch/2020/covid19-liste-non-exhaustive-alternatives-produits-gafam/) de François Charlet

## D'autres offres Cloud

+ [Liiiibre](https://indiehosters.net/) est destiné aux organisations allant d’une dizaine à plusieurs centaines de personnes. Que vous démarriez dans la collaboration en ligne ou que vous soyez un·e usager·ère exigeant·e habitué·e à Google Drive, Slack, et consort souhaitant reprendre le contrôle sur vos données, Liiibre s’adresse à vous.
+ [cryptpad.fr/](https://cryptpad.fr/)

## Analytics

Difficile de ne pas utiliser la solution de G., il existe pourtant des alternatives:

+ [Plausible](https://plausible.io/) - Une solution que vous pouvez également héberger
+ [Matomo](https://fr.matomo.org/)

## Recherche

+ Voir la page sur les [alternatives à Google Search](/ressources/alternatives-a-google-search/)
+ Des [moteurs de recherches dédiés](/ressources/moteurs-aux-resultats-pertinents/) pour répondre à notre quête d'information

## Messagerie

+ [matrix.org/](https://matrix.org/) - An open network for secure, decentralized communication - avec les clients:
+ [Element](https://element.io/) - Secure and independent communication, connected via Matrix
+ [FluffyChat](https://fluffychat.im/) - pas dispo en France?

## Documentation collaborative

+ [Hedgedoc](https://demo.hedgedoc.org/)

## Fil photo

+ [https://pixelfed.fr/](https://pixelfed.fr/)
