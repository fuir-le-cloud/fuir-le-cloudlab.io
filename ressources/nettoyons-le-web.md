---
layout: page.liquid
title: Nettoyons le Web
tags:
  - perspective
---
Nettoyons le Web
=======

Développeur·ses, il est temps pour vous de choisir son camp : allez-vous aider à débarrasser le Web du tracking qui attente à notre vie privée, ou en être complice ?

+ [Clean Up The Web](https://cleanuptheweb.org/fr/)
