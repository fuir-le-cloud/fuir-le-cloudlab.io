---
layout: page.liquid
title: Échanges de pair à pair
date: 2016-01-02
tags:
  - approche
---
Échanges de pair à pair
=======

À cause du protocole BitTorrent, le P2P est devenu le symbole du piratage de fichiers alors qu'il est une solution simple de synchronisation et d'échange. Et cela sans avoir besoin d'encombrer des serveurs tiers.

## Solution de synchronisation

+ [Syncthing](https://syncthing.net/)
+ [Resilio](https://www.resilio.com/) (payant)

## Beaker

Faire un point sur le [navigateur Beaker](https://beakerbrowser.com/) et son approche maligne d'un internet P2P.
