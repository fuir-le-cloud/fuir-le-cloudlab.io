---
layout: page.liquid
title: Moteurs aux résultats pertinents
tags:
  - outil
---
Moteurs aux résultats pertinents
=======

Un début de liste (très personnelle) de moteurs de recherche dont les résultats peuvent être plus pertinents que ceux de Google. À compléter et à documenter.

## Dictionnaires

+ [CNRTL définitions](https://www.cnrtl.fr/definition/)
+ [CNRTL étymologie](https://www.cnrtl.fr/etymologie/)

Vous pouvez utiliser l'[extension CNRTL](https://addons.mozilla.org/fr/firefox/addon/cnrtl-addon/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search) dans Firefox afin de pouvoir chercher la définition d'un mot sur CNRTL par simple control-clic.

## Encyclopédies

+ [Wikipédia en français](https://fr.wikipedia.org/)

(Note: Faire un point sur Kiwix pour utiliser wikipédia en local.)

## Cartes et itinéraires

+ [Geoportail](https://www.geoportail.gouv.fr/)
+ [OpenStreetMap](https://www.openstreetmap.org/)
+ [openrouteservice.org](https://maps.openrouteservice.org/)


## Revues de sciences humaines et sociales

+ [OpenEdition](https://openedition.org/) — portail de ressources électroniques en sciences humaines et sociales.
+ [Isidore](https://isidore.science/) - Votre assistant de recherche en Sciences Humaines et Sociales

## Typographie

+ [Fonts in Use](https://fontsinuse.com/) - An independent archive of typography.
+ [MyFonts](https://www.myfonts.com/) - MyFonts offers the largest selection of professional fonts for any project.
+ [WhatTheFont](https://www.myfonts.com/WhatTheFont/) - Instant font identification powered by the world’s largest collection of fonts.
+ [Font Squirrel](https://www.fontsquirrel.com/) - Free fonts have met their match. We know how hard it is to find quality freeware that is licensed for commercial work. We've done the hard work!

## Vous voulez vraiment faire une recherche globale?

+ Voir la page sur les [alternatives à Google Search](/ressources/alternatives-a-google-search/)
