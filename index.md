---
layout: main.liquid
title: Pourquoi fuir le Cloud ?
tags:
---
# Pourquoi fuir le *Cloud* ?

*Cette documentation fait suite à une discussion sur le Slack des designers éthiques au sujet de possibles alternatives à Google Search et par extension de possibles alternatives à l'ensemble de l'offre GAFAM.*

Les recherches d'alternatives aux GAFAM peuvent être motivées par un besoin de sobriété, un respect des données personnelles ou tout autre volonté éthique ou écologique qui pourtant ne semblent pas être prioritaires pour les acteurs du Net.

Mais le problème semble plus vaste que cette prise de position face aux GAFAM. N'est-il pas une remise en question des façons d'appréhender du savoir et des outils numérisés mais surtout des façons d'appréhender du savoir et des outils *mis à distance* ?

-------

&rarr; Lire [Le manifeste polémique d’UbuWeb](https://www.cairn.info/revue-multitudes-2019-3-page-155.htm) de Kenneth Goldsmith, Traduit de l’anglais (USA) par Cristiano de Sá Fagundes
Dans Multitudes 2019/3 (n° 76) - ce texte sera toujours d'actualité.

-------

## Changer d'outils

<ul>
{%- for post in collections.outil -%}
<li><a href="{{ post.url }}">{{ post.data.title }}</a></li>
{%- endfor -%}
</ul>


## Changer d'approches

<ul>
{%- for post in collections.approche -%}
<li><a href="{{ post.url }}">{{ post.data.title }}</a></li>
{%- endfor -%}
</ul>

## Changer de perspectives

<ul>
{%- for post in collections.perspective -%}
<li><a href="{{ post.url }}">{{ post.data.title }}</a></li>
{%- endfor -%}
</ul>
